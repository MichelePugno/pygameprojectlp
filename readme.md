# Pygame Project


Full documentation on Game and Framework could be find at: https://michelepugno.gitlab.io/pygameprojectlp/

Compressed library for SpaceSimulator can be downloaded from: https://gitlab.com/MichelePugno/pygameprojectlp/-/jobs/artifacts/master/browse?job=pages

The pypi package can be install by downloading the tar.gz located in public/dist folder. Then running `pip install name-of-the-pypi-package.tar.gz` (replacing `name-of-the-pypi-package.tar.gz` with the name of the tar.gz you have downloaded)

The game is released as an executable file (multiplatform) located at https://gitlab.com/MichelePugno/pygameprojectlp/-/jobs/artifacts/master/browse?job=pages under the zip file 'spaceSim.zip'.
Extract the file and run the game. You can also modify the game by editing the 'custom.json' file located in the same folder.

- src/
    - SpaceSimulator: it is a package providing the necessary classes to develop a 2D space game.
    - Game: package for relative gamification
