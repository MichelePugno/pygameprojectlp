"""Simple demo of the controll of the Spaceship, it can fire (with spacebar) and get damaged"""
from Game.UserControlledSpaceship import UserControlledSpaceship
from Game.GameScenario import GameScenario
import pygame

pygame.init()
instance = GameScenario(screen= pygame.display.set_mode((1366,768)),debug=True)

user = UserControlledSpaceship(
    instance, [300, 300], debug=True, lock_on=True, controlled=True, acceleration=100, life=100, max_speed=100, ammo=100, fire_rate=2)
instance.all.add(user)

instance.main_loop()
