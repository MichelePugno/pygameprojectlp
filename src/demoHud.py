"""Simple demo of the HUD, with life and ammo"""

from Game.GameScenario import GameScenario
from SpaceSimulator.SimObject import SimObject
from Game.UserControlledSpaceship import UserControlledSpaceship
from Game.Hud import Hud
import random
import pygame

pygame.init()

# Setting up the screen
instance = GameScenario(screen= pygame.display.set_mode((1366,768)), debug=True)

user = UserControlledSpaceship(
    instance, [300, 300], debug=True, lock_on=False, controlled=True, acceleration=1000, life=100, max_speed=10000, ammo=100, fire_rate=5)
instance.all.add(user)
hud = Hud(instance, user)

for x in range(10):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    image = pygame.Surface([50, 50], pygame.SRCALPHA, )
    image.fill([100, 0, 255])

    test = SimObject(instance, position, image)
    test.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]

    instance.objects.add(test)
instance.main_loop()
