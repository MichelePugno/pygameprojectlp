"""Simple simulation with Particle Generators"""

from SpaceSimulator.Scenario import Scenario
from SpaceSimulator.SimObject import SimObject
from SpaceSimulator.ParticleGenerator import StandardGenerator, CircularGenerator

import random
import pygame

screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

class star(SimObject):

    def __init__(self, scenario: Scenario, position: list,
                 generator, pulse=1, debug=True, lock_on=False) -> None:
        image = pygame.Surface([200, 200])
        image.fill([0, 0, 0])
        image.set_colorkey((0, 0, 0))
        #pygame.draw.circle(image, [255, 255, 0], [4, 4], 5)
        super().__init__(scenario, position, image, debug, lock_on)
        self.gen = generator
        self.pulse = pulse
        self.counter = 0

    def update(self, *args):
        super().update(*args)
        if self.counter == self.pulse:
            self.gen.generate()
            self.counter = 0
        self.counter += 1


pygame.init()

# Setting up the screen
instance = Scenario(screen=screen, screen_size=screen_size, debug=True)

'''testing the stars sprites with standard gen'''
position = [instance.map_area[0]/2,
            instance.map_area[1]/2]
gen = StandardGenerator(instance, position, colors=[
                        (255, 80, 0), (255, 150, 0)], size=[1, 1])
test = star(instance, position, gen)
instance.all.add(test)

'''testing the stars sprites with circular gen'''
position = [instance.map_area[0]/2 + 300,
            instance.map_area[1]/2]
gen = CircularGenerator(instance, position, colors=[(
    255, 80, 0), (255, 150, 0)], size=[1, 1], t2l=10)
test = star(instance, position, gen, pulse=60)

instance.all.add(test)
instance.main_loop()
