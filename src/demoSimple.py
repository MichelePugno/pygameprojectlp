"""Simple simulation"""

from SpaceSimulator.Scenario import Scenario
from SpaceSimulator.SimObject import SimObject
import random
import pygame

screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)
pygame.init()

# Setting up the screen
instance = Scenario(screen = screen, screen_size=screen_size, debug=True)

'''
TODO: take demos as Tests for single units
- stars sprite test
- simObject generic test (stars-asteroids ...)
- camera lock generic test
- boundedScenario generic test
'''

'''testing the stars sprites'''
for x in range(400):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    image = pygame.Surface([10, 10], pygame.SRCALPHA, )
    image.fill([0, 0, 255])
    test = SimObject(instance, position, image)

    test.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
    instance.all.add(test)
instance.main_loop()
