"""Particle object"""

import pygame
import random
from SpaceSimulator import SimObject
from SpaceSimulator import Scenario


class Particle(SimObject.SimObject):
    """Particle object used to improve visual experience

    Particles are part of the particles sprite group

    Args:
        scenario (Scenario): The belonging to scenario
        position (list): initial position
        color (tuple, optional): the color of the particle. Defaults to (255, 0, 0).
        size (list, optional): size of the particle. Defaults to [5, 5].
        max_time2live (int, optional): maximum lifespan of the particle in seconds. Defaults to 1.
        debug (bool, optional): as in SimObject. Defaults to False.
        lock_on (bool, optional): as in SimObject. Defaults to False.
    """

    def __init__(self, scenario: Scenario,
                 position: list,
                 color=(255, 0, 0),
                 size=[5, 5],
                 max_time2live=1,
                 debug=False, lock_on=False) -> None:

        image = pygame.Surface(size, pygame.SRCALPHA)
        image.fill(color)
        self.time2live = random.random() * max_time2live
        super().__init__(scenario, position, image, debug, lock_on)

    def update(self, *args):
        self.time2live -= 1 * self._scenario.delta_time
        if self.time2live < 0:
            self.kill()
        super().update(self)
