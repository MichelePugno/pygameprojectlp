from Game import Game, Asteroid
from Game.UserControlledSpaceship import UserControlledSpaceship
from Game.GameScenario import GameScenario

import pygame
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

def test_game_asteroids():

    main = Game.GameMain(setting="./tests/game/default")

    # check defaults loaded
    assert(main.debug==True)
    assert(main.testing==True) 
    assert(main.map_width==4000)
    assert(main.map_heigth==6000)
    assert(type(main.instance)==GameScenario)
    assert(main.data["asteroids"]["amount"]==5000)

    # check user loaded
    assert(type(main.user)==UserControlledSpaceship)
    assert(main.user.acceleration==80)
    assert(main.user.max_speed==300)
    assert(main.user._life==100)
    assert(main.user._ammo==100)
    assert(main.user._fire_rate==10)
    assert(main.user.debug==True)

def test_game_no_asteroids():

    main = Game.GameMain(setting="./tests/game/custom")

    # check defaults loaded
    assert(main.debug==True)
    assert(main.testing==True) 
    assert(main.map_width==4000)
    assert(main.map_heigth==6000)
    assert(type(main.instance)==GameScenario)
    assert(main.data["asteroids"]["amount"]==0)

    # check user loaded
    assert(type(main.user)==UserControlledSpaceship)
    assert(main.user.acceleration==80)
    assert(main.user.max_speed==300)
    assert(main.user._life==100)
    assert(main.user._ammo==100)
    assert(main.user._fire_rate==10)
    assert(main.user.debug==True)

    # check fire
    pygame.K_UP = 0
    pygame.K_DOWN = 1
    pygame.K_LEFT = 2
    pygame.K_RIGHT = 3
    pygame.K_SPACE = 4
    main.user.update([0,0,0,0,1])
    assert(len(main.instance.bullets.sprites())==1)
    assert(main.user._ammo == 99)
    assert(main.instance.bullets.sprites()[0].velocity==[1000.0, 1000.0])

def test_game_no_asteroids():
    main = Game.GameMain(setting="./tests/game/custom")

    # check defaults loaded
    assert(main.debug==True)
    assert(main.testing==True) 
    assert(main.map_width==4000)
    assert(main.map_heigth==6000)
    assert(type(main.instance)==GameScenario)
    assert(main.data["asteroids"]["amount"]==0)

    # check user loaded
    assert(type(main.user)==UserControlledSpaceship)
    assert(main.user.acceleration==80)
    assert(main.user.max_speed==300)
    assert(main.user._life==100)
    assert(main.user._ammo==100)
    assert(main.user._fire_rate==10)
    assert(main.user.debug==True)

    position = main.user.position
    test = Asteroid.Asteroid(main.instance, position)
    main.instance.all.add(test)

    # check damage
    for x in range(10):
        main.instance.main_logic()

    assert(main.user._life < 100)
