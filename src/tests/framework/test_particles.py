"""Simple simulation with shines"""

from SpaceSimulator.Scenario import Scenario
from SpaceSimulator.SimObject import SimObject
from SpaceSimulator.ParticleGenerator import StandardGenerator, CircularGenerator
from SpaceSimulator.Particle import Particle
import pygame
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)


class star(SimObject):

    def __init__(self, scenario: Scenario, position: list,
                 generator, debug=False, lock_on=False) -> None:
        image = pygame.Surface([10, 10])
        image.fill([0, 0, 0])
        image.set_colorkey((0, 0, 0))
        pygame.draw.circle(image, [255, 255, 0], [4, 4], 5)
        super().__init__(scenario, position, image, debug, lock_on)
        self.gen = generator

    def update(self, *args):
        super().update(*args)
        self.gen.generate()


def test_particle():
    pygame.init()

    # Setting up the screen
    instance = Scenario(screen = screen, screen_size=screen_size, testing=True)

    '''testing the stars sprites with standard gen'''
    position = [instance.map_area[0]/2,
                instance.map_area[1]/2]
    gen = StandardGenerator(instance, position, colors=[
                            (255, 80, 0), (255, 150, 0)], size=[1, 1])
    test = star(instance, position, gen)
    instance.all.add(test)

    '''testing the stars sprites with circular gen'''
    position = [instance.map_area[0]/2 + 300,
                instance.map_area[1]/2]
    gen = CircularGenerator(instance, position, colors=[
                            (255, 80, 0), (255, 150, 0)], size=[1, 1])
    test = star(instance, position, gen)

    instance.all.add(test)
    instance.all.update()

    assert(len(instance.particles.sprites()) > 0)

    for elem in instance.particles.sprites():
        assert(type(elem) == Particle)
