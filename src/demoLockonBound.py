"""This demo shows the lockon camera feature and the controllability of an object in a bounded Scenario.
The green object does not move, the red object with the text is moving, controlled by user and followed by the camera."""

from Game.GameScenario import GameScenario
from SpaceSimulator.BoundedScenario import BoundedScenario
from SpaceSimulator.SimObject import SimObject
from SpaceSimulator.UserControlled import UserControlled
from SpaceSimulator.Scenario import Scenario
import random
import pygame

screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

class Static(SimObject):
    def __init__(self, scenario: Scenario, position, image: pygame.Surface, debug=False, lock_on=False) -> None:
        super().__init__(scenario, position, image, debug, lock_on)

    def __str__(self):
        return "I am not moving"


pygame.init()
instance = BoundedScenario(screen=screen, screen_size=screen_size,debug=True)
instance.map_area = [1500, 1200]

for x in range(200):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    image = pygame.Surface([10, 10], pygame.SRCALPHA, )
    image.fill([0, 0, 255])
    test = SimObject(instance, position, image)

    test.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
    instance.all.add(test)

image = pygame.Surface([20, 20], pygame.SRCALPHA)
pygame.draw.polygon(image, color=(255, 255, 0), points=[
                    (10, 0), (0, 20), (20, 20)])
test = UserControlled(instance, [300, 300], pygame.transform.rotate(
    image, -90), debug=True, lock_on=True, controlled=True)
test.acceleration = 250.0
test.max_speed = 300.0
instance.all.add(test)
image = pygame.Surface([10, 10], pygame.SRCALPHA, )
image.fill([0, 255, 0])
test = Static(instance, [500, 500], image, debug=True)
instance.all.add(test)
test = Static(instance, [1000, 500], image, debug=True)
instance.all.add(test)


instance.main_loop()
