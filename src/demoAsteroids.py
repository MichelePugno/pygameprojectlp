"""Demo of asteroids"""

from Game import GameScenario, Asteroid, UserControlledSpaceship
from Game import Hud
import random
import pygame

# screen = pygame.display.set_mode((1366,768))

pygame.init()

# Setting up the screen
instance = GameScenario.GameScenario(screen= pygame.display.set_mode((1800,900)), debug=True)

user = UserControlledSpaceship.UserControlledSpaceship(
    instance, [instance.screen_size[0] // 2, instance.screen_size[1] // 2])
instance.all.add(user)

hud = Hud.Hud(instance, user)

'''creating lots of asteroids'''
for x in range(20):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    test = Asteroid.Asteroid(instance, position)

instance.main_loop()
