#!/bin/bash

function generate_doc() {
    echo "Generating documentation for $1"
    for f in `ls $1 -1 | sed -e 's/\.py$//'`
    do
        pydoc3 -w $1.$f
    done
    mkdir -p ../public/doc/$1    
    pydoc3 -w $1
    mv *.html ../public/doc/$1/ 2>/dev/null || true
    find ../public/doc/$1 -name '*.__*' -exec rm {} \;
}

generate_doc SpaceSimulator
generate_doc Game
